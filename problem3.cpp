#include "fstream"
#include <vector>

using namespace std;

ifstream in ("input.txt");
ofstream out ("output.txt");

int A, N, buff;
vector<int> arr;

int main(){
	//Read
	in >> N >> A;
	for (int i = 0; i < N; ++i)
	{
		in >> buff;
		arr.push_back(buff);
	}
	//Before
	for (int i = 0; i < N; ++i) if (arr[i] < A) out << arr[i] << " ";
	for (int i = 0; i < N; ++i) if (arr[i] >= A) out << arr[i] << " ";
	return 0;
}