#include "fstream"
#include <iomanip>
#include <cmath>

using namespace std;

ifstream in ("input.txt");
ofstream out ("output.txt");

struct Point{
	int x, y;
};

struct Line{
	Point a, b;
};

bool isAccording(Line a, Line b, float x, float y){
	int minx = min(a.a.x, min(a.b.x, min(b.a.x, b.b.x)));
	int maxx = max(a.a.x, max(a.b.x, max(b.a.x, b.b.x)));
	int miny = min(a.a.y, min(a.b.y, min(b.a.y, b.b.y)));
	int maxy = max(a.a.y, max(a.b.y, max(b.a.y, b.b.y)));
	if ((minx <= x) && (maxx >= x) && (miny <= y) && (maxy >= y)){
		return true;
	} else {
		return false;
	}
}

//Find lines`s intersection
bool isIntersection(Line a, Line b, float &x, float &y){
	int A1, B1, C1, A2, B2, C2, D;
	A1 = a.a.y - a.b.y;
	B1 = a.b.x - a.a.x;
	C1 = -A1 * a.a.x -B1 * a.a.y;
	A2 = b.a.y - b.b.y;
	B2 = b.b.x - b.a.x;
	C2 = -A2 * b.a.x -B2 * b.a.y;
	D = A1*B2-A2*B1;
	if (D == 0){
		return false;
	} else {
		x = -((C1*B2-C2*B1) / (A1*B2-A2*B1));
		y = -((A1*C2-A2*C1) / (A1*B2-A2*B1));
		return isAccording(a, b, x, y);
	}
}

float x, y;
Line line1, line2;

int main(){
	//Read
	in >> line1.a.x >> line1.a.y >> line1.b.x >> line1.b.y >> line2.a.x >> line2.a.y >> line2.b.x >> line2.b.y;
	if (((line1.a.x == line1.b.x) && (line1.a.y == line1.b.y)) || ((line2.a.x == line2.b.x) && (line2.a.y == line2.b.y))){
		if ((line1.a.x <= line2.a.x) && (line1.b.x >= line2.b.x) && (line1.a.y <= line2.a.y) && (line1.b.y >= line2.b.y)){
			if ((line1.a.x == line1.b.x) && (line1.a.y == line1.b.y)){
				out << "Yes" << endl << fixed << setprecision(9) << line1.a.x << " " << line1.a.y;
			} else {
				out << "Yes" << endl << fixed << setprecision(9) << line2.a.x << " " << line2.a.y;
			}
		}
	}
	else if (isIntersection(line1, line2, x, y)){
		out << "Yes" << endl << fixed << setprecision(9) << x << " " << y;
	} else {
		out << "No";
	}
	return 0;
}