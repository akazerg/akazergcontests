#include "fstream"

using namespace std;

ifstream in ("input.txt");
ofstream out ("output.txt");

int N, buff, maxv, maxbuff;
int timing[1000000];

int main(){
	in >> N;
	maxv = 0;
	for (int i = 0; i < 1000000; ++i) timing[buff] = 0;
	for (int i = 0; i < N; ++i)
	{
		in >> buff;
		if (timing[buff] == 0){
			timing[buff] = i;
		} else {
			maxbuff = i - timing[buff];
			timing[buff] = i;
			if (maxv < maxbuff) maxv = maxbuff;
		}
	}
	out << maxv;
	return 0;
}